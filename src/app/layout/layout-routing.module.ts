import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ListarComponent } from './listar/listar.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'listar', component: ListarComponent, pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
