import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {

  nombre: any;
  json:any;

  nombreUser: any;

  constructor(private userService: UsersService) {
    this.nombreUser = "";
  }

  ngOnInit(): void {
    this.obtener();
    this.nombre = "Piero";
    // this.json = [
    //   {  
    //     "name":       "Piero",   
    //     "salary":      1000,   
    //     "married":    true
    //   },
    //   {  
    //     "name":       "Marcos",   
    //     "salary":      10000,   
    //     "married":    false  
    //   },
    //   {  
    //     "name":       "Luis",   
    //     "salary":      2000,   
    //     "married":    false  
    //   },
    //   {  
    //     "name":       "Maria",   
    //     "salary":      50000,   
    //     "married":    true  
    //   }]
  }


   obtener(){
    this.userService.getUsers().subscribe(
      res=>{
        console.log(res.data);
        this.json = res.data
      },
      err=>{
        console.log(err);
      }
    )
  }

}
