import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  url: any;
  constructor(private http: HttpClient) { 
    this.url = 'https://gorest.co.in/public/v1';
  }

  getUsers(): Observable<any>{
    return this.http.get(`${this.url}/users`);
  }
}
